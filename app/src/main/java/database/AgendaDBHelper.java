package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;
public class AgendaDBHelper extends SQLiteOpenHelper{

    private static final String TEXT_TYPE="TEXT";
    private static  final String INTEGER_TYPE="INTEGER";
    private static  final String COMA=" ,";
    private static  int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="agenda.db";


    public AgendaDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
